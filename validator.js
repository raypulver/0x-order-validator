'use strict';

const V2_CONTRACT_ADDRESS = '0x4f833a24e1f95d70f028921e27040ca56e09ab0b';

const {
  toBuffer,
  stripHexPrefix,
  addHexPrefix,
  ecrecover,
  bufferToHex,
  hashPersonalMessage,
  publicToAddress
} = require('ethereumjs-util');

const {
  soliditySha3,
  fromAscii
} = require('web3-utils');

const leftPadZero = (s, n) => Array(Math.max(n - s.length + 1, 1)).join('0') + s;

const from0xSig = (sig) => {
  const stripped = stripHexPrefix(sig);
  const [
    v,
    r,
    s,
    type
  ] = [
    [ 0, 2 ],
    [ 2, 64 ],
    [ 66, 64 ],
    [ 130, 2 ]
  ].map(([ s, length ], i) => (i === 0 || i === 3) ? Number(addHexPrefix(stripped.substr(s, length))) : toBuffer(addHexPrefix(stripped.substr(s, length))));
  return {
    v,
    r,
    s,
    type
  };
};

const EIP712_ORDER_SCHEMA_HASH = soliditySha3(...[
  'Order(',
  'address makerAddress,',
  'address takerAddress,',
  'address feeRecipientAddress,',
  'address senderAddress,',
  'uint256 makerAssetAmount,',
  'uint256 takerAssetAmount,',
  'uint256 makerFee,',
  'uint256 takerFee,',
  'uint256 expirationTimeSeconds,',
  'uint256 salt,',
  'bytes makerAssetData,',
  'bytes takerAssetData',
  ')'
].map((v) => ({
  t: 'string',
  v
})));

const addressToBytes32 = (address) => addHexPrefix(leftPadZero(stripHexPrefix(address), 64));

const recoverOrderSigner = (o) => {
  const rawHash = soliditySha3(...[{
    t: 'bytes32',
    v: EIP712_ORDER_SCHEMA_HASH
  }, {
    t: 'bytes32',
    v: addressToBytes32(o.makerAddress)
  }, {
    t: 'bytes32',
    v: addressToBytes32(o.takerAddress)
  }, {
    t: 'bytes32',
    v: addressToBytes32(o.feeRecipientAddress)
  }, {
    t: 'bytes32',
    v: addressToBytes32(o.senderAddress)
  }, {
    t: 'uint256',
    v: o.makerAssetAmount
  }, {
    t: 'uint256',
    v: o.takerAssetAmount
  }, {
    t: 'uint256',
    v: o.makerFee
  }, {
    t: 'uint256',
    v: o.takerFee
  }, {
    t: 'uint256',
    v: o.expirationTimeSeconds
  }, {
    t: 'uint256',
    v: o.salt
  }, {
    t: 'bytes32',
    v: soliditySha3({
      t: 'bytes',
      v: o.makerAssetData
    })
  }, {
    t: 'bytes32',
    v: soliditySha3({
      t: 'bytes',
      v: o.takerAssetData
    })
  }]);
  const eip712Hashed = soliditySha3(...[{
    t: 'string',
    v: '\x19\x01'
  }, {
    t: 'bytes32',
    v: soliditySha3({
      t: 'bytes32',
      v: soliditySha3(...[
        'EIP712Domain(',
        'string name,',
        'string version,',
        'address verifyingContract',
        ')'
      ].map((v) => ({
        t: 'string',
        v
      })))
    }, {
      t: 'bytes32',
      v: soliditySha3({
        t: 'bytes',
        v: fromAscii('0x Protocol')
      })
    }, {
      t: 'bytes32',
      v: soliditySha3({
        t: 'bytes',
        v: fromAscii('2')
      })
    }, {
      t: 'bytes32',
      v: addressToBytes32(V2_CONTRACT_ADDRESS)
    })
  }, {
    t: 'bytes32',
    v: rawHash
  }]); 
  const {
    v,
    r,
    s,
    type
  } = from0xSig(o.signature);
  const eip712Buffer = toBuffer(eip712Hashed);
  return bufferToHex(publicToAddress(ecrecover(type === 3 ? hashPersonalMessage(eip712Buffer) : eip712Buffer, v, r, s)));
};

const validateOrder = (o) => {
  const signer = recoverOrderSigner(o);
  return signer.toLowerCase() === o.makerAddress.toLowerCase();
};

module.exports = {
  validateOrder,
  recoverOrderSigner
};
