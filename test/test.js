'use strict';

const validator = require('../validator');
const { expect } = require('chai');
const msg = require('../signed');

describe('recovers the right address', () => {
  it('should ecrecover', () => {
    expect(validator.recoverOrderSigner(msg.order)).to.eql(msg.order.makerAddress);
  });
});
